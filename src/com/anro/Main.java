package com.anro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random r = new Random();
        //Здесь мы присваиваем случайный размер базовому массиву.
        int size = r.nextInt(254) + 1;
        ArrayList<Integer> baseArray = new ArrayList<>();
        //заполняем базовый массив различными числами, так,
        // чтобы максимальное число было больше размера массива.
        for (int i = 0; i < size; i++) {
            baseArray.add(i, i + 2);
        }
        ArrayList<Integer> newArray = new ArrayList<>();
        //добавляем в новый массив базовый массив дважды,
        // чтобы получить по 2 экземпляра каждого числа.
        newArray.addAll(baseArray);
        newArray.addAll(baseArray);
        //перемешиваем получившийся массив.
        Collections.shuffle(newArray);
        //убираем из массива случайный элемент.
        System.out.println(newArray.remove(r.nextInt(size)));
        //находим потерянный элемент
        System.out.println(findLostElement(newArray));
    }
    public static int findLostElement (ArrayList<Integer> list){
        //поскольку в итоговом массиве каждое число было записано по 2 раза,
        // то для того, чтобы найти потерянный элемент, нужно найти число,
        // встречающееся нечётное количество раз
        int result = 0;
        for (int i = 0; i < list.size(); i++) {
            int num = list.get(i);
            int count = 0;
            for (int j = 0; j < list.size(); j++) {
                if (num == list.get(j)){
                    count++;
                }
            }
            if (count % 2 != 0){
                result = num;
                break;
            }
        }
        return result;
    }
}
